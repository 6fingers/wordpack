/**
 * 
 */
package com.sixfingers.wordpack;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;

import com.sixfingers.wordpack.model.WordImpl;
import com.sixfingers.wordpack.model.WordSessionArrayList;
import com.sixfingers.wordpack.model.datasource.TableWordsDataSource;

/**
 * @author tatung2112
 * 
 */
public class WordPackWidgetProvider extends AppWidgetProvider {
	private final static String LOGTAG = "WordPackWidgetPrivider";
	public static String WORD_PACK_WIDGET_UPDATE = "com.sixfingers.wordpack.WORD_PACK_WIDGET_UPDATE";
	private static WordSessionArrayList<WordImpl> wordList;

	private static TableWordsDataSource dtaSrcTableWord;

	private Intent packListActivityIntent;
	private PendingIntent packListActivityPendingIntent;

	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager,
			int[] appWidgetIds) {
		final int N = appWidgetIds.length;
		Log.i(LOGTAG, "Updating widgets " + Arrays.asList(appWidgetIds));
		// Perform this loop procedure for each App Widget that belongs to this
		// provider
		for (int i = 0; i < N; i++) {
			int appWidgetId = appWidgetIds[i];
			// Create an Intent to launch MainActivity
			packListActivityIntent = new Intent(context,
					WpPackListFragment.class);
			packListActivityPendingIntent = PendingIntent.getActivity(context, 0,
					packListActivityIntent, 0);
			// Get the layout for the App Widget and attach an on-click listener
			// to the button
			RemoteViews views;
			if (wordList != null && wordList.size() > 0) {
				views = new RemoteViews(context.getPackageName(),
						R.layout.word_pack_widget_1);
			} else {
				views = new RemoteViews(context.getPackageName(),
						R.layout.word_pack_widget_0);
			}
			views.setOnClickPendingIntent(R.id.rootLinearLayout, packListActivityPendingIntent);
			// To update a label
			// Tell the AppWidgetManager to perform an update on the current app
			// widget
			appWidgetManager.updateAppWidget(appWidgetId, views);
		}
	}

	@Override
	public void onEnabled(Context context) {
		super.onEnabled(context);
		Log.d(LOGTAG,
				"Widget Provider enabled. Starting timer to update widget every second");
		
		packListActivityIntent = new Intent(context,
				WpPackListFragment.class);
		packListActivityPendingIntent = PendingIntent.getActivity(context, 0,
				packListActivityIntent, 0);
		
		dtaSrcTableWord = new TableWordsDataSource(context);
		dtaSrcTableWord.open();
		updateWordSessionArrayList();
		AlarmManager alarmManager = (AlarmManager) context
				.getSystemService(Context.ALARM_SERVICE);
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(System.currentTimeMillis());
		calendar.add(Calendar.SECOND,
				WordPackConfigParams.WIDGET_REPEATING_INTERVAL_MILLIS / 1000);
		alarmManager.setRepeating(AlarmManager.RTC, calendar.getTimeInMillis(),
				WordPackConfigParams.WIDGET_REPEATING_INTERVAL_MILLIS,
				createClockTickIntent(context));

	}

	@Override
	public void onDisabled(Context context) {
		super.onDisabled(context);
		Log.d(this.toString(), "Widget Provider disabled. Turning off timer");
		AlarmManager alarmManager = (AlarmManager) context
				.getSystemService(Context.ALARM_SERVICE);
		alarmManager.cancel(createClockTickIntent(context));
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		super.onReceive(context, intent);
		Log.d(this.toString(), "Received intent: " + intent);
		if (WORD_PACK_WIDGET_UPDATE.equals(intent.getAction())) {
			Log.d(this.toString(), "Clock update");
			ComponentName thisAppWidget = new ComponentName(
					context.getPackageName(), getClass().getName());
			AppWidgetManager appWidgetManager = AppWidgetManager
					.getInstance(context);
			for (int appWidgetId : appWidgetManager
					.getAppWidgetIds(thisAppWidget)) {
				updateAppWidget(context, appWidgetManager, appWidgetId);
			}
		}
	}

	public void onWidgetClick(View view) {
		Intent goToPackListIntent = new Intent(view.getContext(),
				WpPackListFragment.class);
		view.getContext().startActivity(goToPackListIntent);
	}

	public static void updateWordSessionArrayList() {
		Log.d(LOGTAG, "updateWordSessionArrayList");
		if (dtaSrcTableWord != null) {
			List<WordImpl> values = dtaSrcTableWord
					.getAllWordInPack(WordPackConfigParams.widgetPackName);
			wordList = new WordSessionArrayList<WordImpl>(values);
		}
	}

	private void updateAppWidget(Context context,
			AppWidgetManager appWidgetManager, int appWidgetId) {
		RemoteViews remoteView;
		Log.d(this.toString(), "updateAppWidget");

		if (wordList != null && wordList.size() > 0) {
			remoteView = new RemoteViews(context.getPackageName(),
					R.layout.word_pack_widget_1);
			WordImpl curWord = wordList.getCurWord();
			remoteView.setTextViewText(R.id.txtSpelling, curWord.getSpelling());
			remoteView.setTextViewText(R.id.txtPhonetics, (String) curWord
					.getPhonetics().get("ON")
					+ " "
					+ (String) curWord.getPhonetics().get("KUN"));
			remoteView.setTextViewText(R.id.txtMeaning, curWord.getMeaning());
		} else {
			remoteView = new RemoteViews(context.getPackageName(),
					R.layout.word_pack_widget_0);
		}
		remoteView.setOnClickPendingIntent(R.id.rootLinearLayout, packListActivityPendingIntent);
		appWidgetManager.updateAppWidget(appWidgetId, remoteView);
	}

	private PendingIntent createClockTickIntent(Context context) {
		Intent intent = new Intent(WORD_PACK_WIDGET_UPDATE);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0,
				intent, PendingIntent.FLAG_UPDATE_CURRENT);
		return pendingIntent;
	}

}
