/**
 * 
 */
package com.sixfingers.wordpack;

/**
 * @author tatung2112
 * 
 */
public class WordPackConfigParams {
	public static int WIDGET_REPEATING_INTERVAL_MILLIS = 5000;
	public static float WIDGET_UPDATE_WORD_SESSION_ARRAY_LIST_HOURS = (float) 0.00111;
	public static String widgetPackName;
}
