package com.sixfingers.wordpack;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class WpStudyFragment extends Fragment {
	private View layout;
	/** Called when the activity is first created. */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// super.onCreate(savedInstanceState);
		layout = inflater.inflate(R.layout.wp_study, null);
		return layout;
	}

}
