package com.sixfingers.wordpack;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.ListFragment;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.View;

import com.viewpagerindicator.TabPageIndicator;

public class WordPackMainActivity extends FragmentActivity {

	// Tab title
	private static final String[] CONTENT = new String[] { "Packs", "Study",
			"Import" };

	// Pager and pager adapter
	private ViewPager pager;
	private FragmentPagerAdapter adapter;

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_word_pack_main, menu);
		return true;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_word_pack_main);

		adapter = new WordPackTabAdapter(getSupportFragmentManager());

		pager = (ViewPager) findViewById(R.id.pager);
		pager.setAdapter(adapter);

		TabPageIndicator indicator = (TabPageIndicator) findViewById(R.id.indicator);
		indicator.setViewPager(pager);
	}


	class WordPackTabAdapter extends FragmentPagerAdapter {
		public WordPackTabAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			if (position == 0) {
				return new WpPackListFragment();
			} else if (position == 1 || position == 2) {
				return new WpStudyFragment();
			}else{
				return null;
			}
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return CONTENT[position % CONTENT.length].toUpperCase();
		}

		@Override
		public int getCount() {
			return CONTENT.length;
		}
	}
}
