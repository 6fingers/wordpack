package com.sixfingers.wordpack;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import com.sixfingers.wordpack.model.PackImpl;
import com.sixfingers.wordpack.model.datasource.TablePacksDataSource;

public class WpPackListFragment extends ListFragment {

	private TablePacksDataSource dtaSrcTablePack;
	private EditText txtPackname;
	private ImageButton btnAddPack;
	private ArrayAdapter<PackImpl> adapter;
	private View layout;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// super.onCreate(savedInstanceState);
		layout = inflater.inflate(R.layout.wp_pack_list, null);
		return layout;
	}

	@Override
	public void onActivityCreated(Bundle savedState) {
		super.onActivityCreated(savedState);
		txtPackname = (EditText) layout.findViewById(R.id.txtPackname);
		btnAddPack = (ImageButton) layout.findViewById(R.id.btnAddPack);

		dtaSrcTablePack = new TablePacksDataSource(layout.getContext());
		dtaSrcTablePack.open();

		List<PackImpl> values = dtaSrcTablePack.getAllPackImpl();
		adapter = new ArrayAdapter<PackImpl>(layout.getContext(),
				android.R.layout.simple_list_item_1, values);
		setListAdapter(adapter);
		registerForContextMenu(getListView());
		registerViewListener();

	}

	private void registerViewListener() {
		/**
		 * set listener for button Add pack
		 */
		btnAddPack.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				String packName = txtPackname.getText().toString();
				if (packName != null && !packName.isEmpty()) {
					PackImpl packImpl = dtaSrcTablePack.createPackImpl(
							packName, "JPN", "ENG", null);
					adapter.add(packImpl);
					txtPackname.setText("");
					adapter.notifyDataSetChanged();
				}
			}
		});
	}

	@Override
	public void onListItemClick(ListView l, View v, int pos, long id) {
		Intent wordListIntent = new Intent(layout.getContext(),
				WpWordListFragment.class);
		wordListIntent.putExtra("selectedPackName", adapter.getItem(pos)
				.toString());
		startActivity(wordListIntent);
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		if (v.getId() == android.R.id.list) {
			ListView.AdapterContextMenuInfo info = (ListView.AdapterContextMenuInfo) menuInfo;
			MenuInflater menuInflater = this.getActivity().getMenuInflater();

			menuInflater
					.inflate(R.menu.word_pack_menu_activity_pack_list, menu);
			menu.setHeaderTitle(adapter.getItem(info.position).toString());
		}

	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
				.getMenuInfo();

		switch (item.getItemId()) {
		case R.id.itmDelete:
			dtaSrcTablePack.deletePackImpl(adapter.getItem(info.position));
			adapter.remove(adapter.getItem(info.position));
			adapter.notifyDataSetChanged();
			return true;
		case R.id.itmSetAsWidgetPack:
			WordPackConfigParams.widgetPackName = adapter
					.getItem(info.position).getPackName();
			WordPackWidgetProvider.updateWordSessionArrayList();
		}
		return false;
	}

	@Override
	public void onResume() {
		dtaSrcTablePack.open();
		super.onResume();
	}

	@Override
	public void onPause() {
		dtaSrcTablePack.close();
		super.onPause();
	}

}
