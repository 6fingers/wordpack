package com.sixfingers.wordpack;

import java.util.List;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.sixfingers.wordpack.model.PackImpl;
import com.sixfingers.wordpack.model.datasource.TablePacksDataSource;

public class WordPackPackListActivity extends ListActivity {

	private TablePacksDataSource dtaSrcTablePack;
	private EditText txtPackname;
	private ArrayAdapter<PackImpl> adapter;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.word_pack_activity_pack_list);

		txtPackname = (EditText) findViewById(R.id.txtPackname);

		dtaSrcTablePack = new TablePacksDataSource(this);
		dtaSrcTablePack.open();

		List<PackImpl> values = dtaSrcTablePack.getAllPackImpl();
		adapter = new ArrayAdapter<PackImpl>(this,
				android.R.layout.simple_list_item_1, values);
		setListAdapter(adapter);
		registerForContextMenu(getListView());
	}

	public void onBtnAddClick(View view) {
		String packName = txtPackname.getText().toString();
		if (packName != null && !packName.isEmpty()) {
			@SuppressWarnings("unchecked")
			ArrayAdapter<PackImpl> adapter = (ArrayAdapter<PackImpl>) getListAdapter();
			PackImpl packImpl = null;
			packImpl = dtaSrcTablePack.createPackImpl(packName, "JPN", "ENG",
					null);
			adapter.add(packImpl);
			txtPackname.setText("");
			adapter.notifyDataSetChanged();
		}
	}

	@Override
	public void onListItemClick(ListView l, View v, int pos, long id) {
		Intent wordListIntent = new Intent(getApplicationContext(),
				WordPackWordListActivity.class);
		wordListIntent.putExtra("selectedPackName", adapter.getItem(pos).toString());
		startActivity(wordListIntent);
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		if (v.getId() == android.R.id.list) {
			ListView.AdapterContextMenuInfo info = (ListView.AdapterContextMenuInfo) menuInfo;
			MenuInflater menuInflater = getMenuInflater();
			menuInflater.inflate(R.menu.word_pack_menu_activity_pack_list, menu);
			menu.setHeaderTitle(adapter.getItem(info.position).toString());
		}

	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
	    AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
	            .getMenuInfo();
	 
	    switch (item.getItemId()) {
	    case R.id.itmDelete:
	    	dtaSrcTablePack.deletePackImpl(adapter.getItem(info.position));
	        adapter.remove(adapter.getItem(info.position));	      
	        adapter.notifyDataSetChanged();
	        return true;
	    case R.id.itmSetAsWidgetPack:
	    	WordPackConfigParams.widgetPackName = adapter.getItem(info.position).getPackName();
	    	WordPackWidgetProvider.updateWordSessionArrayList();
	    }
	    return false;
	}

	@Override
	protected void onResume() {
		dtaSrcTablePack.open();
		super.onResume();
	}

	@Override
	protected void onPause() {
		dtaSrcTablePack.close();
		super.onPause();
	}

}
