package com.sixfingers.wordpack.model;

public class PackImpl {
	private long id;
	private String packName;
	private String srclang;
	private String desLang;
	private String packDescript;
	
	public PackImpl() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public String toString(){
		return packName;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPackName() {
		return packName;
	}

	public void setPackName(String packName) {
		this.packName = packName;
	}

	public String getSrclang() {
		return srclang;
	}

	public void setSrclang(String srclang) {
		this.srclang = srclang;
	}

	public String getDesLang() {
		return desLang;
	}

	public void setDesLang(String desLang) {
		this.desLang = desLang;
	}

	public String getPackDescript() {
		return packDescript;
	}

	public void setPackDescript(String packDescript) {
		this.packDescript = packDescript;
	}

	
}
