package com.sixfingers.wordpack.model;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class WordSessionArrayList<E> extends ArrayList<WordImpl> {
	private int curWordIdx;

	public WordSessionArrayList() {
		super();
		curWordIdx = 0;
	}
	
	public WordSessionArrayList(List<WordImpl> lst){
		super();
		for(WordImpl wordImpl : lst){
			this.add(wordImpl);
		}
		curWordIdx = 0;
	}

	public void shuffle() {
		// TODO: Mix list order
	}

	/**
	 * @return the curWordIdx
	 */
	public int getCurWordIdx() {
		return curWordIdx;
	}	

	public void setCurWordIdx(int curWordIdx) {
		this.curWordIdx = curWordIdx;
	}

	public WordImpl getCurWord() {
		if (curWordIdx >= this.size()) {
			curWordIdx = 0;
		}
		return this.get(curWordIdx++);
	}
}
