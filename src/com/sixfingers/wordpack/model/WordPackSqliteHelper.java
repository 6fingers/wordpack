/**
 * 
 */
package com.sixfingers.wordpack.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * @author tatung2112
 * 
 */
public class WordPackSqliteHelper extends SQLiteOpenHelper {
	
	private static final String LOGTAG = WordPackSqliteHelper.class.getName();

	/*
	 * Table Words, keys: PakcName, Spelling, WordType
	 */
	public static final String TABLE_WORDS = "Words";
	public static final String COLUMN_WORDS_ID = "_id";
	public static final String COLUMN_WORDS_SPELLING = "Spelling";
	public static final String COLUMN_WORDS_PHONETICS = "Phonetics";
	public static final String COLUMN_WORDS_MEANING = "Meaning";
	public static final String COLUMN_WORDS_WORD_TYPE = "WordType";
	public static final String COLUMN_WORDS_LAST_STUDY_TIME = "LastStudyTime";
	public static final String COLUMN_WORDS_STUDY_COUNT = "StudyCount";
	public static final String COLUMN_WORDS_PACK_NAME = "PackName";
	public static final String COLUMN_WORDS_SRC_LANG = "SrcLang";
	public static final String COLUMN_WORDS_DES_LANG = "DesLang";
	
	/*
	 * Table Packs, keys: PackName
	 */
	public static final String TABLE_PACKS = "Packs";
	public static final String COLUMN_PACKS_ID = "_id";
	public static final String COLUMN_PACKS_PACK_NAME = "PackName";
	public static final String COLUMN_PACKS_SRC_LANG = "SrcLang";
	public static final String COLUMN_PACKS_DES_LANG = "DesLang";
	public static final String COLUMN_PACKS_PACK_DESCRIPT = "PackDescript";

	private static final String DB_NAME = "WordPack.db";
	private static final int DB_VERSION = 1;

	private static final String qCreateTablePacks = "CREATE TABLE " + TABLE_PACKS + "(" + 
														COLUMN_PACKS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + 
														COLUMN_PACKS_PACK_NAME + " TEXT NOT NULL, " + 
														COLUMN_PACKS_SRC_LANG + " TEXT, " + 
														COLUMN_PACKS_DES_LANG + " TEXT, " +
														COLUMN_PACKS_PACK_DESCRIPT + " TEXT" +
													");";
	
	private static final String qCreateTableWords = "CREATE TABLE " + TABLE_WORDS + "(" + 
														COLUMN_WORDS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + 
														COLUMN_WORDS_SPELLING + " TEXT NOT NULL, " + 
														COLUMN_WORDS_PHONETICS + " TEXT, " + 
														COLUMN_WORDS_MEANING + " TEXT, " +
														COLUMN_WORDS_WORD_TYPE + " TEXT, " +
														COLUMN_WORDS_LAST_STUDY_TIME + " TEXT, " +
														COLUMN_WORDS_STUDY_COUNT + " INTEGER, " +
														COLUMN_WORDS_PACK_NAME + " TEXT NOT NULL, " +
														COLUMN_WORDS_SRC_LANG + " TEXT NOT NULL, " +
														COLUMN_WORDS_DES_LANG + " TEXT NOT NULL" +
													");";

	/**
	 * @param context
	 */
	public WordPackSqliteHelper(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.database.sqlite.SQLiteOpenHelper#onCreate(android.database.sqlite
	 * .SQLiteDatabase)
	 */
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(qCreateTablePacks);
		db.execSQL(qCreateTableWords);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.database.sqlite.SQLiteOpenHelper#onUpgrade(android.database.sqlite
	 * .SQLiteDatabase, int, int)
	 */
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(LOGTAG, "Upgrade database from version "
				+ oldVersion + " to version " + newVersion
				+ "which will destroy all existing data!");
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_WORDS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_PACKS);
		onCreate(db);
	}

}
