package com.sixfingers.wordpack.model;

import java.util.ArrayList;

public class LanguageType {
	private String langCode;
	private String langName;
	private ArrayList<String> phoneticsTypeName;

	public LanguageType(String langCode, String langName,
			ArrayList<String> phoneticsTypeName) {
		this.langCode = langCode;
		this.langName = langName;
		this.phoneticsTypeName = phoneticsTypeName;
	}

	public String getLangCode() {
		return langCode;
	}

	public void setLangCode(String langCode) {
		this.langCode = langCode;
	}

	public String getLangName() {
		return langName;
	}

	public void setLangName(String langName) {
		this.langName = langName;
	}

	public ArrayList<String> getPhoneticsTypeName() {
		return phoneticsTypeName;
	}

	public void setPhoneticsTypeName(ArrayList<String> phoneticsTypeName) {
		this.phoneticsTypeName = phoneticsTypeName;
	}
}
