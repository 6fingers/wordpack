package com.sixfingers.wordpack.model;

import java.util.Hashtable;

@SuppressWarnings("rawtypes")
public abstract class GeneralWord {
	protected String spelling;
	protected Hashtable phonetics;
	protected String meaning;

	public GeneralWord(String spelling, Hashtable phonetics, String meaning) {
		this.spelling = spelling;
		this.phonetics = phonetics;
		this.meaning = meaning;
	}

	public String getSpelling() {
		return spelling;
	}

	public void setSpelling(String spelling) {
		this.spelling = spelling;
	}

	public Hashtable getPhonetics() {
		return phonetics;
	}

	public void setPhonetics(Hashtable phonetics) {
		this.phonetics = phonetics;
	}

	public String getMeaning() {
		return meaning;
	}

	public void setMeaning(String meaning) {
		this.meaning = meaning;
	}

}
