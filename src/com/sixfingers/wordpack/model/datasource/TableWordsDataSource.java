package com.sixfingers.wordpack.model.datasource;

import java.util.ArrayList;
import java.util.Hashtable;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.sixfingers.wordpack.model.WordImpl;
import com.sixfingers.wordpack.model.WordPackSqliteHelper;

public class TableWordsDataSource extends AbstractWordPackDataSource {

	private final String[] allColumns = { WordPackSqliteHelper.COLUMN_WORDS_ID,
			WordPackSqliteHelper.COLUMN_WORDS_SPELLING,
			WordPackSqliteHelper.COLUMN_WORDS_PHONETICS,
			WordPackSqliteHelper.COLUMN_WORDS_MEANING,
			WordPackSqliteHelper.COLUMN_WORDS_WORD_TYPE,
			WordPackSqliteHelper.COLUMN_WORDS_LAST_STUDY_TIME,
			WordPackSqliteHelper.COLUMN_WORDS_STUDY_COUNT,
			WordPackSqliteHelper.COLUMN_WORDS_PACK_NAME,
			WordPackSqliteHelper.COLUMN_WORDS_SRC_LANG,
			WordPackSqliteHelper.COLUMN_WORDS_DES_LANG };

	public TableWordsDataSource(Context ctx) {
		super(ctx);
		// TODO Auto-generated constructor stub
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	protected <E> Object cursorToDataType(Cursor cursor) {
		String spelling = cursor.getString(cursor
				.getColumnIndex(WordPackSqliteHelper.COLUMN_WORDS_SPELLING));

		Hashtable phonetics = new Hashtable();
		String phoneticsJsonStr = cursor.getString(cursor
				.getColumnIndex(WordPackSqliteHelper.COLUMN_WORDS_PHONETICS));
		JSONObject jsonObj;
		try {
			jsonObj = new JSONObject(phoneticsJsonStr);

			phonetics.put("ON", jsonObj.getString("ON"));
			phonetics.put("KUN", jsonObj.getString("KUN"));

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String meaning = cursor.getString(cursor
				.getColumnIndex(WordPackSqliteHelper.COLUMN_WORDS_MEANING));
		String wordType = cursor.getString(cursor
				.getColumnIndex(WordPackSqliteHelper.COLUMN_WORDS_WORD_TYPE));
		String srcLang = cursor.getString(cursor
				.getColumnIndex(WordPackSqliteHelper.COLUMN_WORDS_SRC_LANG));
		String desLang = cursor.getString(cursor
				.getColumnIndex(WordPackSqliteHelper.COLUMN_WORDS_DES_LANG));
		WordImpl wordImpl = new WordImpl(spelling, phonetics, meaning,
				wordType, srcLang, desLang);
		return wordImpl;
	}

	@SuppressWarnings("rawtypes")
	public WordImpl createWordImpl(String spelling, Hashtable phonetics,
			String meaning, String wordType, String srcLangCode,
			String desLangCode, String packName) {
		ContentValues values = new ContentValues();
		values.put(WordPackSqliteHelper.COLUMN_WORDS_SPELLING, spelling);
		
		JSONObject jsonObj = new JSONObject();
		
		try {
			jsonObj.put("ON", phonetics.get("ON"));
			jsonObj.put("KUN", phonetics.get("KUN"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String phoneticsJsonStr = jsonObj.toString();
		values.put(WordPackSqliteHelper.COLUMN_WORDS_PHONETICS, phoneticsJsonStr);
		values.put(WordPackSqliteHelper.COLUMN_WORDS_MEANING, meaning);
		values.put(WordPackSqliteHelper.COLUMN_WORDS_WORD_TYPE, wordType);
		values.put(WordPackSqliteHelper.COLUMN_WORDS_SRC_LANG, srcLangCode);
		values.put(WordPackSqliteHelper.COLUMN_WORDS_DES_LANG, desLangCode);
		values.put(WordPackSqliteHelper.COLUMN_WORDS_PACK_NAME, packName);
		
		long insertId = database.insert(WordPackSqliteHelper.TABLE_WORDS, null,
				values);
		Cursor cursor = database.query(WordPackSqliteHelper.TABLE_WORDS,
				allColumns, WordPackSqliteHelper.COLUMN_WORDS_ID + " = " + insertId, null,
				null, null, null);
		cursor.moveToFirst();
		WordImpl wordImpl = (WordImpl) cursorToDataType(cursor);
		cursor.close();
		return wordImpl;

	}

	@Override
	public boolean delete(Object data) {
		long id = ((WordImpl) data).getId();
		System.out.println("wordImpl deleted with id = " + id);
		database.delete(WordPackSqliteHelper.TABLE_WORDS,
				WordPackSqliteHelper.COLUMN_WORDS_ID + " = " + id, null);
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<WordImpl> getAllData() {
		ArrayList<WordImpl> wordImplList = new ArrayList<WordImpl>();
		Cursor cursor = database.query(WordPackSqliteHelper.TABLE_WORDS,
				allColumns, null, null, null, null, null);

		cursor.moveToFirst();

		while (!cursor.isAfterLast()) {
			wordImplList.add((WordImpl) cursorToDataType(cursor));
			cursor.moveToNext();
		}

		cursor.close();
		return wordImplList;
	}
	
	public ArrayList<WordImpl> getAllWordInPack(String packName){
		ArrayList<WordImpl> wordImplList = new ArrayList<WordImpl>();
		Cursor cursor = database.query(WordPackSqliteHelper.TABLE_WORDS,
				allColumns, WordPackSqliteHelper.COLUMN_WORDS_PACK_NAME + " = '" + packName + "'", null, null, null, null);

		cursor.moveToFirst();

		while (!cursor.isAfterLast()) {
			wordImplList.add((WordImpl) cursorToDataType(cursor));
			cursor.moveToNext();
		}

		cursor.close();
		return wordImplList;
	}

}
