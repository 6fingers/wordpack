package com.sixfingers.wordpack.model.datasource;

import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.sixfingers.wordpack.model.WordPackSqliteHelper;

public abstract class AbstractWordPackDataSource {
	protected SQLiteDatabase database;
	private final WordPackSqliteHelper dbHelper;
	
	public AbstractWordPackDataSource(Context ctx) {
		dbHelper = new WordPackSqliteHelper(ctx);
	}
	
	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}
	
	abstract protected <E> Object cursorToDataType(Cursor cursor);
	abstract public boolean delete(Object data);
	abstract public <E> List<E> getAllData();
	
}