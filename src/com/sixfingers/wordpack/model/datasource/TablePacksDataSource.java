package com.sixfingers.wordpack.model.datasource;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.sixfingers.wordpack.model.PackImpl;
import com.sixfingers.wordpack.model.WordPackSqliteHelper;

public class TablePacksDataSource {
	private SQLiteDatabase database;
	private final WordPackSqliteHelper dbHelper;
	private final String[] allColumns = { 
			WordPackSqliteHelper.COLUMN_PACKS_ID,
			WordPackSqliteHelper.COLUMN_PACKS_PACK_NAME,
			WordPackSqliteHelper.COLUMN_PACKS_SRC_LANG,
			WordPackSqliteHelper.COLUMN_PACKS_DES_LANG,
			WordPackSqliteHelper.COLUMN_PACKS_PACK_DESCRIPT
			};
	
	public TablePacksDataSource(Context ctx) {
		dbHelper = new WordPackSqliteHelper(ctx);
	}
	
	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}
	
	private PackImpl cursorToPackImpl(Cursor cursor) {
		PackImpl packImpl = new PackImpl();
		packImpl.setId(cursor.getLong(0));
		packImpl.setPackName(cursor.getString(1));
		packImpl.setSrclang(cursor.getString(2));
		packImpl.setDesLang(cursor.getString(3));
		packImpl.setPackDescript(cursor.getString(4));
		return packImpl;
	}

	public PackImpl createPackImpl(String packName, String srcLang, String desLang, String packDescript) {
		ContentValues values = new ContentValues();
		values.put(WordPackSqliteHelper.COLUMN_PACKS_PACK_NAME, packName);
		values.put(WordPackSqliteHelper.COLUMN_PACKS_SRC_LANG, srcLang);
		values.put(WordPackSqliteHelper.COLUMN_PACKS_DES_LANG, desLang);
		values.put(WordPackSqliteHelper.COLUMN_PACKS_PACK_DESCRIPT, packDescript);
		
		long insertId = database.insert(WordPackSqliteHelper.TABLE_PACKS, null,
				values);
		Cursor cursor = database.query(WordPackSqliteHelper.TABLE_PACKS,
				allColumns, WordPackSqliteHelper.COLUMN_PACKS_ID + " = " + insertId, null,
				null, null, null);
		cursor.moveToFirst();
		PackImpl packImpl = cursorToPackImpl(cursor);
		cursor.close();
		return packImpl;
	}

	public void deletePackImpl(PackImpl packImpl) {
		long id = packImpl.getId();
		System.out.println("packImpl deleted with id = " + id);
		database.delete(WordPackSqliteHelper.TABLE_PACKS, WordPackSqliteHelper.COLUMN_PACKS_ID
				+ " = " + id, null);
	}

	public List<PackImpl> getAllPackImpl() {
		ArrayList<PackImpl> packImplList = new ArrayList<PackImpl>();
		Cursor cursor = database.query(WordPackSqliteHelper.TABLE_PACKS,
				allColumns, null, null, null, null, null);

		cursor.moveToFirst();

		while (!cursor.isAfterLast()) {
			packImplList.add(cursorToPackImpl(cursor));
			cursor.moveToNext();
		}

		cursor.close();
		return packImplList;
	}

}
