package com.sixfingers.wordpack.model;

import java.util.Hashtable;

@SuppressWarnings("rawtypes")
public class WordImpl extends GeneralWord {
	private long id;
	private final String srcLangCode;
	private final String desLangCode;
	private final String wordType;

	public WordImpl(String spelling, Hashtable phonetics, String meaning,
			String wordType, String srcLangCode, String desLangCode) {
		super(spelling, phonetics, meaning);
		this.srcLangCode = srcLangCode;
		this.desLangCode = desLangCode;
		this.wordType = wordType;
	}
	
	@Override
	public String toString(){
		return spelling + " - " + wordType + " - "+ phonetics.get("ON") + ", " + phonetics.get("KUN") + " - " + meaning;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	public String getSrcLangCode() {
		return srcLangCode;
	}

	public String getDesLangCode() {
		return desLangCode;
	}

	public String getWordType() {
		return wordType;
	}
	
	

}
