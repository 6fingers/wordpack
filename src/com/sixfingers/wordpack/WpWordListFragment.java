package com.sixfingers.wordpack;

import java.util.Hashtable;
import java.util.List;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.sixfingers.wordpack.model.WordImpl;
import com.sixfingers.wordpack.model.datasource.TableWordsDataSource;

public class WpWordListFragment extends ListActivity {
	private TableWordsDataSource dtaSrcTableWord;
	private EditText txtSpelling;
	private EditText txtWordType;
	private EditText txtPhoneticsOnyomi;
	private EditText txtPhoneticsKunyomi;
	private EditText txtMeaning;

	private String packName;

	private ArrayAdapter<WordImpl> adapter;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.wp_word_list);

		txtSpelling = (EditText) findViewById(R.id.txtSpelling);
		txtWordType = (EditText) findViewById(R.id.txtWordType);
		txtPhoneticsOnyomi = (EditText) findViewById(R.id.txtPhoneticsOnyomi);
		txtPhoneticsKunyomi = (EditText) findViewById(R.id.txtPhoneticsKunyomi);
		txtMeaning = (EditText) findViewById(R.id.txtMeaning);

		dtaSrcTableWord = new TableWordsDataSource(this);
		dtaSrcTableWord.open();
		packName = getIntent().getExtras().getString("selectedPackName");
		List<WordImpl> values = dtaSrcTableWord.getAllWordInPack(packName);
		adapter = new ArrayAdapter<WordImpl>(this,
				android.R.layout.simple_list_item_1, values);
		setListAdapter(adapter);
		packName = getIntent().getExtras().getString("selectedPackName");
		TextView header = (TextView) findViewById(R.id.lblPackName);
		header.setText(packName);
		registerForContextMenu(getListView());

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void onBtnAddClick(View view) {

		if (packName != null && !packName.isEmpty()) {
			ArrayAdapter<WordImpl> adapter = (ArrayAdapter<WordImpl>) getListAdapter();
			WordImpl wordImpl = null;
			Hashtable phonetics = new Hashtable();
			phonetics.put("ON", txtPhoneticsOnyomi.getText().toString());
			phonetics.put("KUN", txtPhoneticsKunyomi.getText().toString());
			wordImpl = dtaSrcTableWord.createWordImpl(txtSpelling.getText().toString(),
					phonetics, txtMeaning.getText().toString(), txtWordType.getText().toString(),
					"JPN", "VIE", packName);
			adapter.add(wordImpl);
			txtSpelling.setText("");
			txtWordType.setText("");
			txtPhoneticsOnyomi.setText("");
			txtPhoneticsKunyomi.setText("");
			txtMeaning.setText("");
			adapter.notifyDataSetChanged();
		}
	}

}
